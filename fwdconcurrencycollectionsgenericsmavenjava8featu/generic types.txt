array
=====
Employee[] emps = new Employee[5];
emps[0] = new Employee(); //Ok
emps[1] = new Student(); //C.E   Incompatible types.  Found: Student, Required: Employee
emps[2] = "aspire";      //C.E   Incompatible types.  Found: String, Required: Employee
Conclusion: Arrays are type-safe.

collection
==========
ArrayList emps = new ArrayList();
emps.add(new Employee()); //Ok
emps.add(new Student()); //Ok
emps.add("aspire"); //Ok
conclusion: collections are non type-safe.
The main limitation with non-generic collection is "No Gaurantee" for element types in collection hence may throw ClassCastException.

Generic types (jdk1.5)
======================
The purpose of generic type is to convert non typ-safe collection into type-safe collection.
ArrayList<Employee> emps = new ArrayList<Employee>();
emps.add(new Employee()); //Ok
emps.add(new Student()); //C.E Incompatible types.  Found: Student, Required: Employee
emps.add("aspire"); //C.E Incompatible types.  Found: String, Required: Employee
conclusion: After generic types, the collection is type-safe.

Conversion (Refactor) from non generic code to generic code is simple.

Mixing Generic and Non-generic types
====================================
Conversion (Refactoring) from non generic code to generic code is possible but tedious. Hence sun allows mixing of non-generic and generic code.

Module1 (old)
-------------
public void meth1(List names){  //List names = new ArrayList<String>();

}

ArrayList names = new ArrayList();
meth2(names); //Ok but gives warning

Module2 (new)
-------------
ArrayList<String> names = new ArrayList<String>();
meth1(names); //Ok but gives warning

public void meth2(List<String> names){  //List<String> names = new ArrayList()

}

List names = new ArrayList<String>(); //Ok but gives warning
List<String> names = new ArrayList(); //Ok but gives warning
Conclusion: One side is generic type but other side is non-generic type.

Q) How JVM allows one side is non-generic type but other side is generic type?
Ans) Inorder to allow mixing of non-generic code with generic code, the generic type info is removed by compiler, which is called as Type Erasure. 
Hence at runtime generic type info is not available i.e., at runtime, both non-generic and generic code look exactly same.
Hence JVM allows mixing of non-generic code with generic code.
Note: Generic types give only compile time protection but not runtime protection.

That means, the generic type info is not available at runtime but Polymorphism needs runtime info. 
Then how polymorphism is achieved for generic types?

Polymorphism w.r.t arrays
=========================
Arrays are polymorphic because arrays are protected at runtime.
Example:
class Animal{}
class Dog extends Animal{}
class Cat extends Animal{}

class Test{
	public static void addAnimal(Animal[] animals){ 
		animals[1] = new Dog(); //ok
		animals[2] = new Cat(); //The code is compiled but throws ArrayStoreException
		

		/*
		animals[1] = new Cat(); //ok
		animals[2] = new Dog(); //throws ArrayStoreException	
		*/				

		/*
		animals[1] = new Dog(); //ok
		animals[2] = new Cat(); //ok
		*/
	}

	public static void main(String[] args){
		Dog[] dogs = new Dog[3];		
		dogs[0] = new Dog(); //Ok
		dogs[1] = new Cat(); //C.E since arrays are protected at compile time
		addAnimal(dogs); //Ok

		Cat[] cats = new Cat[3];
		cats[0] = new Cat(); //Ok
		cats[1] = new Dog(); //C.E since arrays are protected at compile time
		addAnimal(cats); //Ok

		Animal[] animals = new Animal[3];
		animals[0] = new Dog(); //Ok
		animals[1] = new Cat(); //Ok
		addAnimal(animals); //Ok
	}
}

Conclusion: Arrays are polymorphic. Hence following declarations are correct.
Animal[] animals = new Dog[3]; //correct
Animal[] animals = new Cat[3]; //correct
Animal[] animals = new Animal[3]; //correct

Polymorphism w.r.t Generic types
================================
By default generic types are non-polymorphic because generic type information is not available at runtime i.e., generic types are not protected at runtime.
Example:
class Animal{}
class Dog extends Animal{}
class Cat extends Animal{}

class Test{
	public static void addAnimal(List<Animal> animals){ //List<Animal> animals = new ArrayList<Dog>(); //compilation error
		animals.add(new Dog()); //ok
		animals.add(new Cat()); //Ok
	}

	public static void main(String[] args){
		List<Dog> dogs = new ArrayList<Dog>();
		dogs.add(new Dog()); //Ok
		dogs.add(new Cat()); //C.E since generic types are protected at compile time
		addAnimal(dogs); //compilation error

		List<Cat> cats = new ArrayList<Cat>();
		cats.add(new Cat()); //Ok
		cats.add(new Dog()); //C.E since generic types are protected at compile time
		addAnimal(cats); //compilation error

		List<Animal> animals = new ArrayList<Animal>();
		animals.add(new Dog()); //Ok
		animals(new Cat()); //Ok
		addAnimal(animals); //Ok
	}
}

List<Animal> animals = new ArrayList<Dog>(); //wrong
List<Animal> animals = new ArrayList<Cat>(); //wrong
List<Animal> animals = new ArrayList<Animal>(); //correct
Conclusion: By default, the Generic types are non polymorphic.

<? extends generic-type>
=========================
Achieves Generic type polymorphism only for read operation but not for write operation.
Example:
class class Animal{}
class Dog extends Animal{}
class Cat extends Animal{}

class WildcardDemo1{
	public static void readAnimal(List<? extends Animal> animals){ //List<Animal> animals = new ArrayList<Dog>();  //ok
		sop(animals.get(0)); //ok
		sop(animals.get(1)); //ok
		//animals.add(new Dog()); //compilation error
		//animals.add(new Cat()); //compilation error
	}

	public static void main(String[] args){
		List<Dog> dogs = new ArrayList<Dog>();
		dogs.add(new Dog()); //Ok
		dogs.add(new Cat()); //C.E since generic types are protected at compile time
		readAnimal(dogs); //Ok

		List<Cat> cats = new ArrayList<Cat>();
		cats.add(new Cat()); //Ok
		cats.add(new Dog()); //C.E since generic types are protected at compile time
		readAnimal(cats); //ok

		List<Animal> animals = new ArrayList<Animal>();
		animals[0] = new Dog(); //Ok
		animals[1] = new Cat(); //Ok
		readAnimal(animals); //Ok
	}
}
conclusion: Generic types are polymorphic to perform read operation but not for add opertation.
List<? extends Animal> animals = new ArrayList<Dog>(); //ok
List<? extends Animal> animals = new ArrayList<Cat>(); //ok
List<? extends Animal> animals = new ArrayList<Animal>(); //ok
That means List<? extends Animal> allows List<Animal>, List<Dog> and List<Cat> but not List<Object>. Hence polymorphism is achieved only for read operation.

Example:
List<String> list1 = Arrays.asList(1, 2);
List<String> list2 = Arrays.asList(3, 4);
list1.addAll(list2); //Read from list2 and then appended to list1

<? super generic-type>
======================
Allows both read and write operations.
Example:
import java.util.ArrayList;
import java.util.List;
class class Animal{}
class Dog extends Animal{}
class Cat extends Animal{}

public class WildcardDemo2 {
	public static void addDogs(List<? super Dog> dogs){ //It allows List<Dog>, List<Animal> and List<Object>
		dogs.add(new Dog());
		//dogs.add(new Cat()); //Not Ok
	}
	
	public static void addCats(List<? super Cat> cats){ //It allows List<Cat>, List<Animal> and List<Object>
		cats.add(new Cat());
		//cats.add(new Dog()); //Not Ok
	}
	
	public static void addAnimals(List<? super Animal> animals){ //List<? extends Animal> animals = new ArrayList<Dog>();
		animals.add(new Dog()); //ok
		animals.add(new Cat()); //ok 
	}
	
	public static void main(String[] args) {
		List<Dog> dogs = new ArrayList<Dog>();
		dogs.add(new Dog()); //Ok
		addDogs(dogs); //Ok
		
		List<Cat> cats = new ArrayList<Cat>();
		cats.add(new Cat()); //Ok
		addCats(cats); //ok
		
		List<Animal> animals = new ArrayList<Animal>();
		animals.add(new Cat());//Ok
		animals.add(new Dog());//Ok
		addDogs(animals); //Ok
		addCats(animals); //Ok
	}
}
Note:
1) List<? super Dog> allows List<Dog>, List<Animal> and List<Object> but not List<Cat>.
2) List<? super Cat> allows List<Cat>, List<Animal> and List<Object> but not List<Dog>.
3) List<? super Animal> allows List<Animal> and List<Object> but not List<Dog> and List<Cat>.
All above operations are applicable for add operation.

Real-time scenario for Generic classes
======================================
//without generic class
public interface StudentDao {
	public void create(Student s);
	public Student read(int no);
	public void update(Student s);
	public void delete(int no);
	//finder methods
}

public interface EmployeeDao {
	public void create(Employee s);
	public Employee read(short no);
	public void update(Employee s);
	public void delete(short no);
	//finder methods
}
------------------------------

//With Generic class
public interface GenericDao <T, ID extends Serializable> {
	public void create(T t){}
	public T read(ID no){}
	public void update(T t){}
	public void delete(ID no){}	
}

public interface StudentDao extends GenericDao<Student, Integer>{
	//finder methods
}

public interface EmployeeDao extends GenericDao<Employee, Short>{
	//finder methods
}	

